const request = require('supertest');
const app = require('./app');

describe('usability', () => {

    it('when request /status, should return 200', async () => {
        const res = await request(app)
            .get('/status');
        expect(res.statusCode).toEqual(200);
    });

    it('Request to transfer $100, should return 200 with success message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer', amount: '100'})
        expect(res.statusCode).toEqual(200);
        expect(res.text).toContain('Successfully transfered $100');
    });

    it('Request to withdraw $100, should return 400 with an error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'withdraw', amount: '100'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('Only transfer allowed');
    });


});
